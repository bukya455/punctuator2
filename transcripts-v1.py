# import modules
from flask import Flask, render_template, request
import textwrap

from youtube_transcript_api import YouTubeTranscriptApi
from punctuator import Punctuator

# load model
punctuator2 = Punctuator('Demo-Europarl-EN.pcl')
# p.punctuate('some text')

# helper function
# first check youtube video has manual transcripts if yes extract otherwise use our stt model(In progress)
def getTranscripts(input_video_id):
        '''
        For rendering results on GUI
        '''
        pattern = 'https://www.youtube.com/watch?v='
        if pattern in input_video_id:
                video_id = input_video_id.split(pattern)[1]
        else:
                video_id = input_video_id
                        
        #list all transcripts which are available for a given video
        transcript_list = YouTubeTranscriptApi.list_transcripts(video_id)
        transcript = transcript_list.find_transcript(['en'])

        # manual transrcipts
        if transcript.is_generated == False:
                manual_transcripts = transcript_list.find_manually_created_transcript(['en']) # manual trnscripts
                final_transripts = manual_transcripts.fetch() # output transcripts
                output_text = []
                for text in final_transripts:
                        temp_text = text['text']
                        output_text.append(temp_text)
                final_text = ' '.join(output_text).replace('[Applause]', '').replace('[Music]', '')
                final_text = final_text.replace('\n', ' ').strip()
                # textwrap to improve readability
                final_text = textwrap.dedent(final_text).strip()
                final_text = textwrap.fill(final_text, width=135)
                info1 = '[Info] Manual Transcripts Are Extracted:\n'
                videoids = transcript.video_id + '\n'
                id_info = '[Info] Your YouTube Id is: '
                final_text = id_info + videoids + '\n' + info1 + '\n' + final_text
                # Manual transcripts are punctuated by default.
                #print("[Info] Manual Transcripts Are Extracted.\n")
                # return render_template('result.html', prediction=my_prediction)
                return final_text

        # generated transcripts
        else:
                generated_transcripts = transcript_list.find_generated_transcript(['en']) # generated trnscripts
                final_transripts = generated_transcripts.fetch() # output transcripts
                output_text = []
                for text in final_transripts:
                        temp_text = text['text']
                        output_text.append(temp_text)
                final_text = ' '.join(output_text).replace('[Applause]', '').replace('[Music]', '')
                final_text = final_text.replace('\n', ' ').strip()
                # add punctuations for auto generated transcripts
                punctuated_trans = punctuator2.punctuate(final_text)
                # textwrap to improve readability
                punctuated_trans = textwrap.dedent(punctuated_trans).strip()
                punctuated_trans = textwrap.fill(punctuated_trans, width=135)
                info2 = '[Info] Auto-Generated English Transcripts Are Extracted And Added Punctuations:\n'
                info3 = '[Info] Sorry, Manual Transcripts Are Not Available For This Video!\n'
                videoids = transcript.video_id + '\n'
                id_info = '[Info] Your YouTube Id is: ' 
                punctuated_trans = id_info + videoids+ '\n' + info3 + '\n' + info2 + '\n' + punctuated_trans
                #print('[Info] Sorry, Manual Transcripts Are Not Available For This Video!\n')
                #print("[Info] Auto-Generated English Transcripts Are Extracted.\n")
                return punctuated_trans


# check samples
print('\n')
print('--------------Generated Sample-----------')
print('\n')
print(getTranscripts('rZ11n4Rbv5I'))
print('\n')

print('--------------Manual Sample-----------')
print('\n')
print(getTranscripts('https://www.youtube.com/watch?v=sKvMxZ284AA'))
