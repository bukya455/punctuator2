# import modules
from flask import Flask, render_template, request

from youtube_transcript_api import YouTubeTranscriptApi
from punctuator import Punctuator

# load model
punctuator2 = Punctuator('Demo-Europarl-EN.pcl')
# p.punctuate('some text')


app = Flask(__name__)

@app.route('/')
def home():
	return render_template('home.html')

@app.route('/predict',methods=['POST'])

# helper function
# first check youtube video has manual transcripts if yes extract otherwise use our stt model(In progress)
def predict():
        '''
        For rendering results on GUI
        '''

        if request.method == 'POST':
                input_text = request.form['text']
                #list all transcripts which are available for a given video
                transcript_list = YouTubeTranscriptApi.list_transcripts(input_text)
                transcript = transcript_list.find_transcript(['en'])

                # manual transrcipts
                if transcript.is_generated == False:
                        manual_transcripts = transcript_list.find_manually_created_transcript(['en']) # manual trnscripts
                        final_transripts = manual_transcripts.fetch() # output transcripts
                        output_text = []
                        for text in final_transripts:
                                temp_text = text['text']
                                output_text.append(temp_text)
                        final_text = ' '.join(output_text).replace('[Applause]', '').replace('[Music]', '')
                        final_text = final_text.replace('\n', ' ')
                        print("[Info] Manual Transcripts Are Extracted.\n")
                        return final_text

                # generated transcripts
                else:
                        generated_transcripts = transcript_list.find_generated_transcript(['en']) # generated trnscripts
                        final_transripts = generated_transcripts.fetch() # output transcripts
                        output_text = []
                        for text in final_transripts:
                                temp_text = text['text']
                                output_text.append(temp_text)
                        final_text = ' '.join(output_text).replace('[Applause]', '').replace('[Music]', '')
                        final_text = final_text.replace('\n', ' ')
                        print('[Info] Sorry, Manual Transcripts Are Not Available For This Video!\n')
                        print("[Info] Auto-Generated English Transcripts Are Extracted.\n")
                        return final_text
        
if __name__ == '__main__':
	app.run(debug=True)
